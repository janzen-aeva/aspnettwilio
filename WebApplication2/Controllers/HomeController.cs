﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using Twilio.TwiML;
using WebApplication2.Models;
using Twilio.AspNet.Core;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public void Send(string message, string number)
        {
            const string account = "AC5b3beda7e14f36db1b102d60069c264c";
            const string authToken = "60acaad425a83753b62ef2cbfd4e2007";

            TwilioClient.Init(account, authToken);

            var send = MessageResource.Create(
                body: message,
                from: new Twilio.Types.PhoneNumber("+19388008385"),
                to: new Twilio.Types.PhoneNumber(String.Format("+1{0}", number))
                );
        }

        public void Call(string number)
        {
            const string account = "AC5b3beda7e14f36db1b102d60069c264c";
            const string authToken = "60acaad425a83753b62ef2cbfd4e2007";

            TwilioClient.Init(account, authToken);

            var to = new PhoneNumber(number);
            var from = new PhoneNumber("+19388008385");
            var call = CallResource.Create(to, from,
                url: new Uri("http://demo.twilio.com/docs/voice.xml"));
        }

        //public IActionResult Privacy()
        //{
        //    return View();
        //}

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
